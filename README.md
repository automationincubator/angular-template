# Template

# Folder Structure

    // end-to-end-tests
    |- e2e/
    |----- app.e2e-spec.ts
    |----- app.po.ts
    |----- tsconfig.e2e.json

    // npm dependencies
    |- node_modules/

    // public facing app. built things go here. this wont show until we run a build
    |- dist/

    // where most of the work will be done
    |- src/
    |----- app/
        |----- app.component.css|html|spec.ts|ts
        |----- app.module.ts
    |----- assets/
    |----- environments/
        |----- environment.prod.ts|ts
    |----- favicon.ico
    |----- index.html
    |----- main.ts
    |----- polyfills.ts
    |----- styles.css
    |----- test.ts
    |----- tsconfig.app.json
    |----- tsconfig.spec.json
    |----- typings.d.ts

    // overall configuration
    |- .angular-cli.json  // the main configuration file
    |- .editorconfig      // editorconfig which is used in some VS Code setups
    |- .gitignore
    |- karma.conf.js
    |- package.json
    |- protractor.conf.js
    |- README.md
    |- tsconfig.json
    |- tslint.json

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
